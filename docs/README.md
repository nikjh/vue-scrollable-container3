# Vue Scrollable container

It's component to create custom scrollable area in your app.
Uses native js methods for scrolling. Has zero dependencies.

Note. This version to use with vue 3 version. [Vue 2 version](https://gitlab.com/nikjh/vue-scrollable-container2).

## Install

:::: code-group
::: code-group-item npm
```sh
npm i vue-scrollable-container3
```
:::
::: code-group-item yarn
```sh
yarn add vue-scrollable-container3
```
:::
::::

## Usage

:::: code-group
::: code-group-item Global
```js
import { createApp } from 'vue';
import VScrollbar from 'vue-scrollable-container3';

import 'vue-scrollable-container3/styles.scss';

createApp().use(VScrollbar).mount('#app');
```
:::
::: code-group-item Local
```vue
<template>
  <VScrollbar>
    Something
  </VScrollbar>
</template>

<script setup lang="ts">
import VScrollbar from 'vue-scrollable-container3/scrollbar';
</script>
```
:::
::::
::: tip
Styles must be added in your main file in each situation
:::

## Props

| Props             | Type      | Default                                                | Description                                                               |
|-------------------|-----------|--------------------------------------------------------|---------------------------------------------------------------------------|
| `tag`             | `String`  | `"div"`                                                | Basic root tag of scrollable area                                         |
| `content-style`   | `Object`  | `{}`                                                   | Custom CSS styles for vs-content                                          |
| `bar-color`       | `String`  | `''`                                                   | Custom color for bars                                                     |
| `offsetting-bar`  | `Boolean` | `true`                                                 | Enable calculating offsets of container for rendering bars by those sizes |
| `observer-config` | `Object`  | `{ attributes: true, childList: true, subtree: true }` | Config for MutationObserver to watch content changes                      |

## Result

```html
<div class="vs-container">
  <div class="vs-wrapper">
    <div class="vs-content">
      Something
    </div>
  </div>
  <div class="vs-scroll vs-scroll--x"></div>
  <div class="vs-scroll vs-scroll--y"></div>
</div>
```

## Author
Nikita Rogachev
