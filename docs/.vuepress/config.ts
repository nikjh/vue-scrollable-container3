import { defineUserConfig, defaultTheme } from 'vuepress'
import { registerComponentsPlugin } from "@vuepress/plugin-register-components";
import { getDirname, path } from '@vuepress/utils'

const __dirname = getDirname(import.meta.url)

export default defineUserConfig({
  lang: 'en-EN',
  title: 'Vue scrollable container 3',
  base: '/vue-scrollable-container3/',
  dest: 'public',
  theme: defaultTheme({
    colorMode: 'light',
    navbar: [
      {
        text: 'Main',
        link: '/',
      },
      {
        text: 'Customization',
        link: '/customization'
      },
      {
        text: 'Demo',
        link: '/demo',
      },
      {
        text: 'Extra',
        children: [
          {
            text: 'Changelog',
            link: '/changelog',
          },
          {
            text: 'License',
            link: '/license'
          }
        ]
      },
    ],
  }),
  plugins: [
    // @ts-ignore
    registerComponentsPlugin({
      componentsDir: path.resolve(__dirname, './components'),
    }),
  ],
});
