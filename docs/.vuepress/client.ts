import { defineClientConfig } from '@vuepress/client'
import datepickerLibrary from '../../main';

export default defineClientConfig({
  enhance({ app, router, siteData }) {
    app.use(datepickerLibrary);
  },
  setup() {},
  rootComponents: [],
});
