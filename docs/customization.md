# Vue scrollable container customization

You can customize component styles as you need via SCSS.

### Default SCSS variables
By default in `setting.scss` contains 4 variables:
1. `$vs-bar-size` - width/height of bar (9px by default);
2. `$vs-bar-border-radius` - rounding size of bar (4px by default);
3. `$vs-bar-color` - color of bars (hsla(214, 82%, 51%, 0.6) by default). Can be rewritten by barColor prop;
4. `$vs-render-css` - flag to enable styles rendering (true by default);

You can rewrite them as you wish.

### Custom mixin
Also, you can extend or total rewrite all given styles;
You can some rewrite default styles via custom scss mixin;
Just add in your main `@mixin custom-v-scrollbar` and it's being applied automatically.

Note. Styles created via mixin doesn't delete default rules. Extend/rewrite only;


### Your own styles
Also, you can switch `$vs-render-css` to `false` or not importing styles file and write fully your own styles;

You need describe next classes:
* `.vs-container` - main container of area;
* `.vs-wrapper` - wrapper of content
* `.vs-content` - content area; also for content exist prop contentStyle to add custom styles for specific scroll area;
* `.vs-scroll` - bar
  * `.vs-scroll--x` - horizontal bar
  * `.vs-scroll--y` - vertical bar

#### Optional
* `.vs-grabbed` - adding to body for indicate grabbing state when bar was grabbed;
* `.vs-container:hover > .vs-scroll, .vs-container:active > .vs-scroll` - visibility of scrollbars on hover or active contain;
