# Changelog
Version created from [vue-scrollable-vue2](https://www.npmjs.com/package/vue-scrollable-container2) for usages in Vue3.
Some improvements added;

### v1.0.0
* Updated customization via styles.

### v0.0.1

#### Styles
* In separated file now;
* usage is more flexible:
  * You can extend/rewrite styles via `custom-v-scrollbar` scss mixin.
  * Bar color can be replaced globally via scss variable; `$vd-bar-color`

#### Vue
* Update code for Vue 3 usage.
* Can use:
  * globally via Vue.use;
  * locally in component via import.

#### Builder
* Use rollup for build library.
