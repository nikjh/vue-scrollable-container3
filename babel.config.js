module.exports = {
  env: {
    build: {
      presets: [
        ['@babel/preset-env', {
          modules: false,
          loose: true,
        }],
        ['vue', {
          eventModifiers: false,
          vModel: false,
        }],
      ],
    },
    test: {
      presets: [
        ['@babel/preset-env', {
          modules: 'cjs',
          loose: true,
        }],
        ['vue', {
          eventModifiers: false,
          vModel: false,
        }],
      ],
    },
  },
  plugins: [
    '@babel/plugin-syntax-jsx',
    '@babel/plugin-proposal-object-rest-spread',
  ],
};
