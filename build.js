const fs = require('fs');
const path = require('path');
const rimraf = require('rimraf');
const { rollup } = require('rollup');

const configFactory = require('./rollup.config');

const dist = path.resolve('./dist');

const removePrefix = (fileName) => fileName.replace(/^v-/, '');

const build = async function build(option) {
  const bundle = await rollup(option.input);
  await bundle.write(option.output);
};

const makeDir = function makeDir(folder) {
  try {
    fs.mkdirSync(folder);
  } catch (e) {
    if (e.code === 'EEXIST') { return folder; }
  }
};

const compileDirectory = (input, output, options = {}) => {
  const {
    extensions,
    modifier,
  } = {
    extensions: ['js', 'vue'],
    modifier: (fileName) => fileName,
    ...options,
  };

  try {
    makeDir(output);

    fs.readdirSync(input)
      .filter((fileName) => extensions.includes(fileName.split('.')[1]))
      .forEach((component) => {
        const name = component.split('.')[0];
        build(configFactory({
          input: path.join(input, component),
          file: `${output}/${modifier(name)}.js`,
          name: modifier(name),
        }));
      });
  } catch (e) {
    console.log(e);
  }
};

(async () => {
  // eslint-disable-next-line no-promise-executor-return
  await new Promise((r) => rimraf(dist, r));

  compileDirectory('./components/', dist, { modifier: removePrefix });

  try {
    build(configFactory({
      input: 'main.js',
      file: `${dist}/main.js`,
      name: 'main',
    }));

    const allStyles = [];
    fs.readdirSync('./theme')
      .filter((file) => file.split('.')[0] !== 'main')
      .forEach((file) => {
        const fileContent = fs.readFileSync(`./theme/${file}`);

        allStyles.push(fileContent);

        fs.writeFileSync(`${dist}/${removePrefix(file)}`, fileContent);
      });
    fs.writeFileSync(`${dist}/styles.scss`, allStyles.join('\n'));
  } catch (e) {
    console.error(e);
  }
})();
