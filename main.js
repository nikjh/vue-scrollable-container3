import VScrollbar from './components/v-scrollbar.vue';

const components = {
  'v-scrollbar': VScrollbar,
};

const plugin = {
  install(Vue) {
    for (const prop in components) {
      if (components.hasOwnProperty(prop)) {
        const component = components[prop];
        Vue.component(component.name || prop, component);
      }
    }
  },
};

export default plugin;
