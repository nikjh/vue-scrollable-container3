# Vue Scrollable container

It's component to create custom scrollable area in your app.
Uses native js methods for scrolling. Has zero dependencies.

Note. This version to use with vue 3 version. [Vue 2 version](https://gitlab.com/nikjh/vue-scrollable-container2).

[DEMO](https://nikjh.gitlab.io/vue-scrollable-container3/demo.html)

## Install

### npm
```sh
npm i vue-scrollable-container3
```
### yarn
```sh
yarn add vue-scrollable-container3
```

## Usage

### As SFC import
```vue
<template>
  <vscrolbar>
    Something
  </vscrolbar>
</template>

<script>
import VScrollbar from 'vue-scrollable-container3/component';

export default {
  components: {
    VScrollbar,
  },
};
</script>
```
### As global registered component
```js
import { createApp } from 'vue';
import VScrollbar from 'vue-scrollable-container3';

import 'vue-scrollable-container3/styles.scss';

createApp().use(VScrollbar).mount('#app');
```

## Props

| Props             | Type      | Default                                                | Description                                                               |
|-------------------|-----------|--------------------------------------------------------|---------------------------------------------------------------------------|
| `tag`             | `String`  | `"div"`                                                | Basic root tag of scrollable area                                              |
| `content-style`   | `Object`  | `{}`                                                   | Custom CSS styles for vs-content                                          |
| `bar-color`       | `String`  | `''`                                                   | Custom color for bars                                                     |
| `offsetting-bar`  | `Boolean` | `true`                                                 | Enable calculating offsets of container for rendering bars by those sizes |
| `observer-config` | `Object`  | `{ attributes: true, childList: true, subtree: true }` | Config for MutationObserver to watch content changes                      |

## Result

```html
<div class="vs-container">
  <div class="vs-wrapper">
    <div class="vs-content">
      Something
    </div>
  </div>
  <div class="vs-scroll vs-scroll--x"></div>
  <div class="vs-scroll vs-scroll--y"></div>
</div>

```

## Notes
**tag**
`tag="section"` return `<section class="vs-conatiner">`
Other parts of structure will not change.

## License
[MIT](LICENSE.md)

## Author
Nikita Rogachev
