const vue = require('rollup-plugin-vue');
const babel = require('@rollup/plugin-babel');
const replace = require('@rollup/plugin-replace');
const commonjs = require('@rollup/plugin-commonjs');
const { terser } = require('rollup-plugin-terser');

module.exports = ({ input, file, name }) => ({
  input: {
    input,
    external: ['vue'],
    plugins: [
      replace({
        ENV: JSON.stringify(process.env.NODE_ENV || 'development'),
        preventAssignment: true,
      }),
      vue({
        preprocessStyles: true,
      }),
      babel({
        exclude: [
          'node_modules/**',
          '.docs/**',
        ],
        extensions: ['.js', '.jsx', '.es6', '.es', '.mjs', '.vue'],
        babelHelpers: 'bundled',
      }),
      commonjs(),
      process.env.NODE_ENV === 'production' && terser({
        toplevel: true,
        compress: true,
      }),
    ],
  },
  output: {
    format: 'umd',
    file,
    name: name || 'main.js',
    exports: 'named',
    minifyInternalExports: process.env.NODE_ENV === 'production',
    globals: {
      vue: 'vue',
    },
  },
});
